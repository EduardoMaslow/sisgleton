/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author aluno
 */
public class Teste {
    
    public static void main(String[] args) {
        VariasInstancias v1= new VariasInstancias();
        VariasInstancias v2= new VariasInstancias();
        VariasInstancias v3= new VariasInstancias();
        VariasInstancias v4= new VariasInstancias();
        VariasInstancias v5= new VariasInstancias();
        
        UmaInstancia u1= UmaInstancia.obter();
        UmaInstancia u2= UmaInstancia.obter();

        System.out.println(v1.getQuant());
        
        System.out.println(u1.getQuant());
    }
}
